﻿using UniRx;
using UnityEngine;


public class UserPanelPresenter : MonoBehaviour {

    [SerializeField]
    private UserPanelView view;
    [SerializeField]
    private UserPanelModel model;

    private const int MIN_LABIRYNTH_SIZE = 10;
    private const string NO_SAVE_FILE = "Save file not found";

    void Start() {

        view.LabirynthSizeInput
            .OnValueChangedAsObservable()
            .Subscribe(text => {
                int value = text.ToInt();
                if (value >= MIN_LABIRYNTH_SIZE) {
                    view.RebuildLabirynthButton.interactable = true;;
                } else {
                    view.RebuildLabirynthButton.interactable = false;
                }
            });

        view.RebuildLabirynthButton
            .OnClickAsObservable()
            .Subscribe(_ => {
                int size = view.LabirynthSizeInput.text.ToInt();
                model.Labirynth = new Labirynth(size);
                view.BlockersSizeInput.gameObject.SetActive(true);
                view.RebuildBlockersButton.gameObject.SetActive(true);

                view.LabirynthSizeInput.gameObject.SetActive(false);
                view.RebuildLabirynthButton.gameObject.SetActive(false);
                view.LoadButton.gameObject.SetActive(false);
            });

        view.BlockersSizeInput
            .OnValueChangedAsObservable()
            .Subscribe(text => {
                int size = text.ToInt();
                if (size > 0) {
                    view.RebuildBlockersButton.interactable = true;
                } else {
                    view.RebuildBlockersButton.interactable = false;
                }
            });

        view.RebuildBlockersButton
            .OnClickAsObservable()
            .Subscribe(_ => {
                int blockCount = view.BlockersSizeInput.text.ToInt();
                for (int i = 0; i < blockCount; i++) {
                    model.BlockerFactory.BlockTile(model.Labirynth);
                }
                model.StartEnd.FindStartEnd(model.Labirynth);

                view.AlgorythmSelectDropdown.gameObject.SetActive(true);
                view.FindWayButton.gameObject.SetActive(true);
                view.SaveButton.gameObject.SetActive(true);

                view.BlockersSizeInput.gameObject.SetActive(false);
                view.RebuildBlockersButton.gameObject.SetActive(false);
            });

        view.AlgorythmSelectDropdown
            .ObserveEveryValueChanged(index => view.AlgorythmSelectDropdown.value)
            .Subscribe(index => {
                if (index > 0) {
                    view.FindWayButton.interactable = true;
                } else {
                    view.FindWayButton.interactable = false;
                }
            });

        view.FindWayButton
            .OnClickAsObservable()
            .Select(index => view.AlgorythmSelectDropdown.value)
            .Subscribe(index => {
                model.Solver.Solve(view.AlgorythmSelectDropdown.options[index].text, model.Labirynth);
                AddPathNotFoundOvbservable();

                view.FindWayButton.gameObject.SetActive(false);
                view.AlgorythmSelectDropdown.gameObject.SetActive(false);
                view.SaveButton.gameObject.SetActive(false);

            });

        view.SaveButton
            .OnClickAsObservable()
            .Subscribe(_ => {
                model.DataPersistor.Save(model.Labirynth);
            });

        view.LoadButton
            .OnClickAsObservable()
            .Subscribe(_ => {
                model.Labirynth = model.DataPersistor.Load();
                if (model.Labirynth == null) {
                    view.ErrorText.text = NO_SAVE_FILE;
                } else{
                    model.Labirynth.Load();

                    view.AlgorythmSelectDropdown.gameObject.SetActive(true);
                    view.FindWayButton.gameObject.SetActive(true);

                    view.LabirynthSizeInput.gameObject.SetActive(false);
                    view.RebuildLabirynthButton.gameObject.SetActive(false);
                    view.LoadButton.gameObject.SetActive(false); 
                }
            });

        TurnOffGameobjects();

    }

    private void AddPathNotFoundOvbservable() {
        model.Solver.CurrentSolver.errorString
                    .ObserveEveryValueChanged(text => model.Solver.CurrentSolver.errorString)
                    .Subscribe(text => {
                        view.ErrorText.text = text;
                    });
    }

    private void TurnOffGameobjects() {
        
        view.BlockersSizeInput.gameObject.SetActive(false);
        view.RebuildBlockersButton.gameObject.SetActive(false);
        view.AlgorythmSelectDropdown.gameObject.SetActive(false);
        view.FindWayButton.gameObject.SetActive(false);
        view.SaveButton.gameObject.SetActive(false);
    }


}
