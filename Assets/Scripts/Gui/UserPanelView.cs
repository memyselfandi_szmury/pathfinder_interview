﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserPanelView : MonoBehaviour {

    public const string DROPDOWN_SELECT = "Select algorithm";
    public const string DROPDOWN_Breadth = "Breadth First";
    public const string DROPDOWN_ASTAR = "A*";

    void Start() {
        algorythmSelectDropdown.AddOptions(
            new List<string> {
                DROPDOWN_SELECT,
                DROPDOWN_Breadth,
                DROPDOWN_ASTAR });
    }

    [SerializeField]
    private InputField labirynthSizeInput;
    public InputField LabirynthSizeInput {
        get { return labirynthSizeInput; }
    }

    [SerializeField]
    private Button rebuildLabirynthButton;
    public Button RebuildLabirynthButton {
        get { return rebuildLabirynthButton; }
    }

    [SerializeField]
    private Text errorText;
    public Text ErrorText {
        get { return errorText; }
    }

    [SerializeField]
    private InputField blockersSizeInput;
    public InputField BlockersSizeInput {
        get { return blockersSizeInput; }
    }

    [SerializeField]
    private Button rebuildBlockersButton;
    public Button RebuildBlockersButton {
        get { return rebuildBlockersButton; }
    }

    [SerializeField]
    private Dropdown algorythmSelectDropdown;
    public Dropdown AlgorythmSelectDropdown {
        get { return algorythmSelectDropdown; }
    }

    [SerializeField]
    private Button findWayButton;
    public Button FindWayButton {
        get { return findWayButton; }
    }

    [SerializeField]
    private Button saveButton;
    public Button SaveButton {
        get { return saveButton; }
    }

    [SerializeField]
    private Button loadButton;
    public Button LoadButton {
        get { return loadButton; }
    }

}
