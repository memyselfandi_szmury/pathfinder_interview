﻿using UnityEngine;

public class UserPanelModel : MonoBehaviour {

    public void Awake() {
        blockerFactory = new BlockerFactory();
        startEnd = new StartEndFactory();
        solver = new SolverContainer();
        dataPersistor = new Persistor();
    }

    private Labirynth labirynth;
    public Labirynth Labirynth {
        get { return labirynth; }
        set { labirynth = value; }
    }

    private IBlockerFactory blockerFactory;
    public IBlockerFactory BlockerFactory {
        get { return blockerFactory; }
    }

    
    private IStartEndFactory startEnd;
    public IStartEndFactory StartEnd {
        get { return startEnd; }
    }

    private ISolverContainer solver;
    public ISolverContainer Solver {
        get { return solver; }
    }

    private IPersistor dataPersistor;
    public IPersistor DataPersistor {
        get { return dataPersistor; }
    }
}
