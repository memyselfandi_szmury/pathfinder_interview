﻿
public class SolverContainer : ISolverContainer {

    private LabirynthSolver solver;
    public LabirynthSolver CurrentSolver{
        get { return solver; }
    }

    public void Solve(string algorythm,Labirynth labirynth) {    
        switch (algorythm) {
            case UserPanelView.DROPDOWN_Breadth:
                solver = new BreadthFirst(labirynth);
                solver.Solve();
                break;
            default:
                solver = new AStar(labirynth);
                solver.Solve();
                break;
        }
    }

}
