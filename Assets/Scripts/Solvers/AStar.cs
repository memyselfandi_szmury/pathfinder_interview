﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class AStar : LabirynthSolver {

    public AStar(Labirynth labirynth) : base(labirynth) {
    }

    public override void Solve() {
        route.Add(labirynth.startTile);
        Extensions.GetCorutineStarter().StartCoroutine(FindWay(labirynth));
        for(int i =0;i < labirynth.Size * labirynth.Size; i++) {
            labirynth.GetTile(i).heurestic = new AStarHeuristic(labirynth.GetTile(i));
        }
        ((AStarHeuristic)labirynth.startTile.heurestic).routeCost = 0;
    }

    protected override IEnumerator FindWay(Labirynth labirynth) {
        while (route.Count > 0 && route[0] != labirynth.endTile) {
            yield return null;
            route[0].MarkTile(Color.gray);

            if (CheckFieldOn(0, 1))
                break;
            if (CheckFieldOn(1, 0))
                break;
            if (CheckFieldOn(0, -1))
                break;
            if (CheckFieldOn(-1, 0))
                break;

            route.RemoveAt(0);
            route = route
                .OrderBy(tile => tile.heurestic.GetTotalCost())
                .ThenBy(tile => tile.heurestic.GetHeuristicCost())
                .ToList();
        }
        if(route.Count == 0) {
            errorString = NO_WAY;
        } else {
            BuildPath(labirynth);
        }

    }

    private bool CheckFieldOn(int xOffset, int yOffset) {
        currentTile = GetTile(route[0].x, route[0].y, xOffset, yOffset);
        if (currentTile != null) {
            currentTile.heurestic.VisitTile(route, labirynth);
        }
        return currentTile == labirynth.endTile;
    }

    private void BuildPath(Labirynth labirynth) {
        
        currentTile = labirynth.endTile;
        while (currentTile.parent != labirynth.startTile) {
            currentTile.MarkTile(Color.blue);
            currentTile = currentTile.parent;
        }
        currentTile.MarkTile(Color.blue);
        currentTile.parent.MarkTile(Color.blue);
    }
}