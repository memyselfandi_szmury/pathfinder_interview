﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarHeuristic : IHeuristic {

    public float enterCost = 1;
    public float totalCost;
    public float distanceCost;
    public float routeCost = float.MaxValue;
    private Tile current;

    public AStarHeuristic(Tile current) {
        this.current = current;
    }

    public bool VisitTile(List<Tile> route, Labirynth labirynth) {
        if (current == labirynth.endTile) {
            current.parent = route[0];
            return true;
        }
        if (route[0].heurestic.GetRouteCost() + enterCost < routeCost) {
            if (!route.Contains(current)) {
                route.Add(current);
            }
            routeCost = route[0].heurestic.GetRouteCost() + enterCost;
            distanceCost = Vector3.Distance(current.plane.transform.position,
                labirynth.endTile.plane.transform.position) * 2f;
            totalCost = routeCost + distanceCost;
            current.parent = route[0];
        }
        return false;
    }

    public float GetHeuristicCost() {
        return distanceCost;
    }

    public float GetTotalCost() {
        return totalCost;
    }

    public float GetRouteCost() {
        return routeCost;
    }


}
