﻿
using System.Collections;
using UnityEngine;

public class BreadthFirst : LabirynthSolver {

    public BreadthFirst(Labirynth labirynth) : base(labirynth) {
    }

    public override void Solve() {
        route.Add(labirynth.startTile);
        for (int i = 0; i < labirynth.Size * labirynth.Size; i++) {
            labirynth.GetTile(i).heurestic = new BreadthFirstHeuristic(labirynth.GetTile(i));
        }
        Extensions.GetCorutineStarter().StartCoroutine(FindWay(labirynth));
    }

    protected override IEnumerator FindWay(Labirynth labirynth) {
        while (route.Count > 0 && route[0] != labirynth.endTile) {
            route[0].MarkTile(Color.gray);

            if (CheckFieldOn(0, 1))
                break;
            if (CheckFieldOn(1, 0))
                break;
            if (CheckFieldOn(0, -1))
                break;
            if (CheckFieldOn(-1, 0))
                break;

            yield return null;
            route.RemoveAt(0);
        }

        if(route.Count  == 0) {
            errorString = NO_WAY;
        } else {
            BuildPath(labirynth);
        }
    }

    private void BuildPath(Labirynth labirynth) {
        currentTile = labirynth.endTile;
        while (currentTile.parent != labirynth.startTile) {
            currentTile.MarkTile(Color.blue);
            currentTile = currentTile.parent;
        }
        currentTile.MarkTile(Color.blue);
        currentTile.parent.MarkTile(Color.blue);
    }

    private bool CheckFieldOn(int xOffset, int yOffset) {
        currentTile = GetTile(route[0].x, route[0].y, xOffset, yOffset);
        if (currentTile != null && !currentTile.visited) {
            currentTile.heurestic.VisitTile(route, labirynth);
        }
        return currentTile == labirynth.endTile;
    }
}
