﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadthFirstHeuristic : IHeuristic {

    private Tile current;
    public BreadthFirstHeuristic(Tile current) {
        this.current = current;
    }

    public float GetHeuristicCost() {
        throw new NotImplementedException();
    }

    public float GetRouteCost() {
        throw new NotImplementedException();
    }

    public float GetTotalCost() {
        throw new NotImplementedException();
    }

    public bool VisitTile(List<Tile> route, Labirynth labirynth) {

        route.Add(current);
        current.parent = route[0];
        current.visited = true;
        return current == labirynth.endTile;
        
    }
}
