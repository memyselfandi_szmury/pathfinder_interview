﻿using System;
using System.Collections;
using System.Collections.Generic;

public abstract class LabirynthSolver {
    protected const string NO_WAY = "Way could not be found";
    public string errorString = "";

    protected List<Tile> route;
    protected Labirynth labirynth;
    protected Tile currentTile;

    public float stepTime;

    private Tile tile;
    public LabirynthSolver(Labirynth labirynth) {
        route = new List<Tile>();
        this.labirynth = labirynth;
    }

    public Tile GetTile(int x,int y, int xOffset,int yOffset) {
        tile = labirynth.GetTile(x + xOffset, y + yOffset);
        if (tile != null) {
            if(tile.passable) {
                return tile;
            }
        }
        return null;
    }

    public abstract void Solve();
    protected abstract IEnumerator FindWay(Labirynth labirynth);

}
