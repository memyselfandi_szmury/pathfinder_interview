﻿
using System;
using UnityEngine;

[Serializable]
public class Tile {
    private const string PREFAB_PATH = "Prefabs/Tile";

    public int x, y;

    public bool visited = false;
    public bool passable;

    [NonSerialized]
    public GameObject plane;
    public Tile parent;

    public IHeuristic heurestic;

    public Tile(int x,int y, GameObject root) {
        this.x = x;
        this.y = y;
        CreateField(root);
        passable = true;
    }

    public void BlockTile() {
        MarkTile(Color.red);
        passable = false;
    }

    public void Load(GameObject root) {
        CreateField(root);
        if (!passable) {
            MarkTile(Color.red);
        }
    }

    public void MarkTile(Color color) {
        plane.GetComponent<Renderer>().material.color = color;
    }

    private void CreateField(GameObject root) {
        plane = GameObject.Instantiate(Resources.Load(PREFAB_PATH) as GameObject);
        plane.transform.position = new Vector3(x, y, 0);
        plane.name = "x: " + x + " y: " + y;
        plane.transform.parent = root.transform;

    }

}
