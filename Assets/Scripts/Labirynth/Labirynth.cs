﻿using UnityEngine;


[System.Serializable]
//This class can't have interface so it can be serialized
public class Labirynth {

    private int size;
    public int Size {
        get { return size; }
    }

    public Tile startTile, endTile;
    private Tile[] tiles;

    private const string ROOT = "root";

    public Labirynth (int size) {
        this.size = size;
        tiles = new Tile[size * size];

        GameObject root = CreateRoot();

        for (int i = 0; i < size * size; i++) {
            tiles[i] = new Tile(i % size, i / size, root);
        }

        PlaceCamera(size);
    }

    public Tile GetTile(int x, int y)
    {
        try
        {
            if (x + y * size < y * size)
                return null;
            if (x + y * size >= size + y * size)
                return null;
            return tiles[(int)(x + y * size)];
        }
        catch
        {
            return null;
        }
    }

    public Tile GetTile(int tile) {
        return tiles[tile];
    }

    public void Load() {
        GameObject root = CreateRoot();
        for (int i = 0; i < size * size; i++) {
            tiles[i].Load(root);
        }
        startTile.MarkTile(Color.green);
        endTile.MarkTile(Color.blue);

        PlaceCamera(size);
    }

    private static GameObject CreateRoot() {
        GameObject root = new GameObject();
        root.name = ROOT;
        return root;
    }


    private void PlaceCamera(int size) {
        Camera.main.transform.position = new Vector3(size / 2, size / 2, -size);
    }
}
