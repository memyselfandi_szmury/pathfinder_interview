﻿
using UnityEngine;

public class StartEndFactory : IStartEndFactory {
    
    public void FindStartEnd(Labirynth labirynth) {
        FindStartTile(labirynth);
        FindEndTile(labirynth);
    }

    private void FindStartTile(Labirynth labirynth) {
        Tile currentTile;
        do {
            currentTile = GetRandomTile(labirynth);
        } while (!currentTile.passable);

        currentTile.MarkTile(Color.green);
        labirynth.startTile = currentTile;
    }

    private void FindEndTile(Labirynth labirynth) {
        Tile currentTile;
        do {
            currentTile = GetRandomTile(labirynth);
        } while (!currentTile.passable && currentTile != labirynth.startTile);

        currentTile.MarkTile(Color.blue);
        labirynth.endTile = currentTile;
    }

    private Tile GetRandomTile(Labirynth labirynth) {
        int ranomIndex = Random.Range(0, labirynth.Size * labirynth.Size);
        return labirynth.GetTile(ranomIndex);
    }
}
