﻿
public abstract class Blocker  {

    public abstract bool  TryToBlock(int x, int y, Labirynth labirynth);

    protected bool TryToBlockTiles(int x, int y, int xoffset, int yoffset, Labirynth labirynth) {
        if (IsInBoundaries(x, y, xoffset, yoffset, labirynth)) {
            if (!CanTileBeBlocked(x, y, xoffset, yoffset, labirynth)) {
                return false;
            }
            BlockTiles(x, y, xoffset, yoffset, labirynth);
            return true;
        }
        return false;
    }

    protected bool CanBlock(int x, int y,Labirynth labirynth) {
        return labirynth.GetTile(x, y).passable;
    }

    protected void Block(int x, int y, Labirynth labirynth) {
        labirynth.GetTile(x, y).BlockTile();
    }

    protected bool IsInBoundaries(int x, int y, 
        int xoffset,int yoffset, Labirynth labirynth) {

        return x <= labirynth.Size - xoffset 
            && y <= labirynth.Size - yoffset;
    }

    protected bool CanTileBeBlocked(int x, int y, int xoffset,int yoffset, Labirynth labirynth) {
        for (int i = 0; i <= xoffset; i++) {
            for (int j = 0; j <= yoffset; j++) {
                if (!CanBlock(x + i, y + j, labirynth)) {
                    return false;
                }
            }
        }
        return true;
    }

    protected void BlockTiles(int x, int y, int xoffset, int yoffset, Labirynth labirynth) {
        for (int i = 0; i <= xoffset; i++) {
            for (int j = 0; j <= yoffset; j++) {
                Block(x + i, y + j, labirynth);
            }
        }
    }

}
