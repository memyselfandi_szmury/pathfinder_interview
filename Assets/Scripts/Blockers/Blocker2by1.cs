﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocker2by1 : Blocker {

    public override bool TryToBlock(int x, int y, Labirynth labirynth) {
        return TryToBlockTiles(x, y, 1, 0, labirynth);
    }
}
