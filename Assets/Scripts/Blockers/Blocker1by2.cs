﻿
public class Blocker1by2 : Blocker {

    public override bool TryToBlock(int x, int y, Labirynth labirynth) {
        return TryToBlockTiles(x, y, 0, 1, labirynth);
    }
}