﻿using UnityEngine;

public class BlockerFactory : IBlockerFactory {

    private const int MAX_TRIES = 20;
    private const int BLOCKER_TYPES = 4;

    private int tries = 0;
    private Blocker[] blockers;

    public BlockerFactory() {
        blockers = new Blocker[4];
        blockers[0] = new Blocker1by1();
        blockers[1] = new Blocker1by2();
        blockers[2] = new Blocker2by1();
        blockers[3] = new Blocker2by2();
    }

    public void BlockTile(Labirynth labirynth) {
        Blocker blocker = GetRandomBlocker();
        tries = 0;
        while (!blocker.TryToBlock(Random.Range(0, labirynth.Size - 1),
            Random.Range(0, labirynth.Size - 1), labirynth)) {

            blocker = GetRandomBlocker();
            if (tries > MAX_TRIES) {
                break;
            }
            tries++;
        }
    }

    private Blocker GetRandomBlocker() {
        return blockers[Random.Range(0, BLOCKER_TYPES)];
    }
}
