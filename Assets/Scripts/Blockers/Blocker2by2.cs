﻿

public class Blocker2by2 : Blocker {

    public override bool TryToBlock(int x, int y, Labirynth labirynth) {
        return TryToBlockTiles(x, y, 1, 1, labirynth);
    }
}
