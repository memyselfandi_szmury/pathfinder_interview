﻿using System;
using UnityEngine;

public static class Extensions {

    public const string CORUTINE_STARTER = "CorutineStarter";

    public static int ToInt(this string value) {
        int outValue;
        Int32.TryParse(value, out outValue);
        return outValue;
    }

    public static MonoBehaviour GetCorutineStarter () {
        return GameObject.Find(CORUTINE_STARTER).GetComponent<CorutineStarter>();
    } 

}
