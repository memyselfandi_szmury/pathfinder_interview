﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Persistor : IPersistor {
    private const string DATA_FILE = "/save.dat";

    private BinaryFormatter formater;
    private FileStream file;

    public Persistor() {
        formater = new BinaryFormatter();
    }

    public void Save(System.Object toSave) {
        file = File.Open(Application.dataPath + DATA_FILE, FileMode.OpenOrCreate);
        formater.Serialize(file, toSave);
        file.Close();
    }

    public Labirynth Load() {
        if (File.Exists(Application.dataPath + DATA_FILE)) {
            file = File.Open(Application.dataPath + DATA_FILE,FileMode.Open);
            return (Labirynth)formater.Deserialize(file);
        }
        return null;
    }

}
