﻿
public interface IPersistor {
    void Save(System.Object toSave);
    Labirynth Load();
}
