﻿using System.Collections.Generic;

public interface IHeuristic {

    float GetTotalCost();
    float GetHeuristicCost();
    float GetRouteCost();

    bool VisitTile(List<Tile> route, Labirynth labirynth);
    
}
