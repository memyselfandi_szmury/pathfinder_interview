﻿
public interface ISolverContainer  {
    void Solve(string algorythm, Labirynth labirynth);
    LabirynthSolver CurrentSolver { get; }
}
